import React from 'react'
import { Button } from 'antd'
import { useDispatch } from 'react-redux'
import { issuesSlice } from '../store/slices/issues'
import { Draggable } from 'react-beautiful-dnd'

const IssueList = ({ issues, status }) => {
  const dispatch = useDispatch()
  const handleRemoveIssue = (issueId) => dispatch(issuesSlice.actions.removeIssue(issueId))
  return (
    <div>
      <h4>{status}</h4>
      {issues
        .filter(issue => issue.status === status)
        .map((issue, index) => (
          <Draggable draggableId={issue.id} key={issue.id} index={index}>
            {(provided, snapshot) => (
              <div ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                className="lin-card has-space-bottom">
                <h4>{issue.desc}</h4>
                <h6>
                  {issue.createdAt}
                </h6>
                <h6>
                  {issue.status}
                </h6>
                <Button onClick={() => handleRemoveIssue(issue.id)} type="danger">
                  Remove
                </Button>
              </div>
            )}
          </Draggable>
        ))
      }
    </div >
  )
}

export default IssueList
