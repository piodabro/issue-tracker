import React from 'react'
import { Layout } from 'antd'
import config from './config'
import AppLogo from './components/AppLogo'
import AddIssue from './components/AddIssue'
import IssueList from './components/IssueList'
import { useSelector, useDispatch } from 'react-redux'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { issuesSlice } from './store/slices/issues'

const { Header, Content } = Layout

const App = () => {
  const { appName, issueStatuses } = config
  const dispatch = useDispatch()
  const issues = useSelector(state => state.issues.issues)

  const onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }

    const newStatus = result.destination.droppableId
    dispatch(issuesSlice.actions.updateStatus({ id: result.draggableId, newStatus: newStatus }))
  }

  return (
    <div>
      <Layout>
        <Header>
          <div className="container">
            <div className="row">
              <div className="col-12">
                <AppLogo appName={appName} />
              </div>
            </div>
          </div>
        </Header>
        <Content>
          <div className="container">
            <div className="row">
              <div className="col-12 has-space-top">
                <div className="lin-card">
                  <AddIssue />
                </div>
              </div>
              <DragDropContext onDragEnd={onDragEnd}>
                {issueStatuses.map(status => (
                  <Droppable droppableId={status} key={status} >
                    {(provided, snapshot) => (
                      <div ref={provided.innerRef}
                        {...provided.droppableProps}
                        className="col-3 has-space-top">
                        <IssueList issues={issues} status={status} />
                        {provided.placeholder}
                      </div>
                    )}
                  </Droppable>
                ))}
              </DragDropContext>
            </div>
          </div>
        </Content>
      </Layout>
    </div>
  )
}

export default App
