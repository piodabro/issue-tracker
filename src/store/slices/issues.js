import { createSlice } from '@reduxjs/toolkit'
import uuid from 'uuid'
import config from '../../config'
import moment from 'moment'

export const issuesSlice = createSlice({
  name: 'issues',
  initialState: {
    issues: []
  },
  reducers: {
    addIssue: (state, action) => {
      const newIssue = {
        desc: action.payload,
        id: uuid.v4(),
        status: config.issueStatuses[0],
        createdAt: moment().toISOString()
      }
      state.issues = [...state.issues, newIssue]
    },
    clearAll: (state, action) => {
      state.issues = []
    },
    removeIssue: (state, action) => {
      state.issues = state.issues.filter(issue => issue.id !== action.payload)
    },
    updateStatus: (state, action) => {
      const issueIndex = state.issues.findIndex(issue => issue.id === action.payload.id)
      state.issues[issueIndex].status = action.payload.newStatus
    }
  }
})

export const { actions } = issuesSlice
