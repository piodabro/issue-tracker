const config = {
  appName: 'Issue Tracker',
  issueStatuses: ['New', 'In Progress', 'Rejected', 'Resolved']
}

export default config
